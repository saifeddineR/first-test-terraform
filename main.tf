terraform {
  required_providers {
    aws = {
      version = ">= 2.7.0"

    }
  }
}

provider "aws" {
  region = "eu-west-3"
}

resource "aws_instance" "instance_test" {

  ami             = var.ami_id
  instance_type   = var.instance_type
  key_name        = "my_key"
  security_groups = ["my_sgrp"]

  tags = {
    Name = "test_app"
  }
}

